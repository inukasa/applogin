import {createStackNavigator, createAppContainer} from 'react-navigation';
import LoginScreen from './src/components/LoginScreen';
import SecondScreen from './src/components/SecondScreen';

const AppNavigator = createStackNavigator({
    Home: LoginScreen,
    Login: SecondScreen
  },
  {
    initialRouteName: "Home"
  }
  );
  export default AppNavigator;
  