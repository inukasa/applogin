//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Login from './Login';

// create a component
export default class LoginScreen extends Component {
    render() {
        return (
                <Login/>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

