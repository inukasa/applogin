//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, StatusBar, Button , Alert, ToastAndroid} from 'react-native';
import {StackNavigator,} from 'react-navigation';

// create a component
export default class LoginForm extends Component {
    constructor(props) {
        super(props);
      
        this.state = {
          username: '',
          password: ''
        };
      }
      _onPressLogin (event) {
        fetch('https://login-api-example.herokuapp.com/users/authenticate/', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                userName: this.state.username,
                passWord: this.state.password
            })
          }).then(res =>res.json())
          .then(response => {
              console.log('Success: ', JSON.stringify(response))
              if(JSON.stringify(response) === '{"code":"11","message":"Login succecssfull!"}'){
                Alert.alert(
                    'Alert Success',
                    'Log in Successfully-ahihi',
                    [
                      {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                      {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }else{
                Alert.alert(
                    'Alert failed',
                    'Sai tài khoản hoặc mật khẩu, nhập lại lẹ lẹ coi !!!',
                    [
                      {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                      {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }
            })
          .then((responseJSON)=>{ })
          .catch(error => console.error('Error:', error));

    }
    render() {
        return (
            <View style={styles.container}>
            <StatusBar barStyle= "light-content"></StatusBar>
                <TextInput style={styles.txtinput}
                placeholder="Username"
                placeholderTextColor='#FFFFFF'
                returnKeyType="next"
                onSubmitEditing={()=>this.passwordInput.focus()}
                keyboardType="email-address"
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText = {(value) => this.setState({username:value})}
                value={this.state.username}
                />
                <TextInput style={styles.txtinput}
                placeholder = "Password"
                placeholderTextColor='#FFFFFF'
                secureTextEntry
                returnKeyType="go"
                ref={(input)=>this.passwordInput=input}
                onChangeText = {(value) => this.setState({password:value})}
                value={this.state.password}
                />
                <TouchableOpacity 
                style={styles.buttonContainer}
                onPress={()=>{
                    fetch('https://login-api-example.herokuapp.com/users/authenticate/', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password
            })
          }).then(res =>res.json())
          .then(response => {
              console.log('Success: ', JSON.stringify(response))
              if(JSON.stringify(response) === '{"code":"11","message":"Login succecssfull!"}'){
                Alert.alert(
                    'Alert Success',
                    'Log in Successfully-ahihi',
                    [
                      {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                      {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }else{
                Alert.alert(
                    'Alert failed',
                    'Sai tài khoản hoặc mật khẩu, nhập lại lẹ lẹ coi !!!',
                    [
                      {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                      {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }
            })
          .then((responseJSON)=>{ })
          .catch(error => console.error('Error:', error));
                }}
                >
                    <Text style={styles.textLogin}>LOGIN</Text>
                    
                </TouchableOpacity>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        padding: 20,
    },
    txtinput: {
        height:40,
        marginBottom: 10,
        backgroundColor: '#3399FF',
        color: '#FFFFFF',
        paddingHorizontal: 10,

    },
    buttonContainer: {
        backgroundColor: '#66FFFF',
        paddingVertical: 15
    },
    textLogin: {
        textAlign: 'center',
        fontWeight: '700',
    }
});

