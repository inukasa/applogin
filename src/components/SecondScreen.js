//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import LoginScreen from './LoginScreen';

// create a component
export default class SecondScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text>SecondScreen</Text>
                <Text>Login Successfullly</Text>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

