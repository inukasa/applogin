//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet,KeyboardAvoidingView } from 'react-native';
import LoginForm from './LoginForm';
import { white } from 'ansi-colors';
// create a component
export default class Login extends Component {
    render() {
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <View style={styles.displayLogin}>
                <Text style={styles.login}>Login</Text>
                </View>
                <View style={styles.form}>
                    <LoginForm/>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FF3366'
    },
    login: {
        color:'#FFFFFF',
        fontSize: 30,
    },
    displayLogin: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    form: {
    },
});

